package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckboxPage extends BasePage {

    public CheckboxPage(WebDriver driver) {
        super(driver);
    }

    public CheckboxPage step (String step) {
        stepAction(step);
        return this;
    }

    private By elementInputInCheckboxPage(String sectionName, String elementName) {
        return By.xpath(String.format("(//div[@class='%s'] | //div[@class='%s']/following-sibling::*[1])//*[normalize-space(.)='%s']/input", sectionName, sectionName, elementName));
    }

    private By elementLabelInCheckboxPage(String sectionName, String elementName) {
        return By.xpath(String.format("(//div[@class='%s'] | //div[@class='%s']/following-sibling::*[1])//*[normalize-space(.)='%s']/label", sectionName, sectionName, elementName));
    }


    public CheckboxPage verifyCheckboxState(String elementName, boolean expected) {
        boolean actual = isChecked(elementInputInCheckboxPage("example", elementName));
        assertEquals(actual, expected);
        return this;
    }

    public CheckboxPage setCheckboxState(String elementName, boolean expected) {
        if (isChecked(elementInputInCheckboxPage("example", elementName)) != expected) {
            click(elementLabelInCheckboxPage("example", elementName));
        }
        return this;
    }

    public CheckboxPage selectRadioButtonInHorizontalSection(String elementName) {
            click(elementLabelInCheckboxPage("inline fields", elementName));
        return this;
    }

    public CheckboxPage verifyRadioButtonInHorizontalSectionSelected(String elementName, boolean expected) {
        boolean actual = isChecked(elementInputInCheckboxPage("inline fields", elementName));
        assertEquals(actual, expected);
        return this;
    }

    public CheckboxPage selectRadioButtonInVerticalSection(String elementName) {
        click(elementLabelInCheckboxPage("grouped fields", elementName));
        return this;
    }

    public CheckboxPage verifyRadioButtonInVerticalSectionSelected(String elementName, boolean expected) {
        boolean actual = isChecked(elementInputInCheckboxPage("grouped fields", elementName));
        assertEquals(actual, expected);
        return this;
    }

    public CheckboxPage setSwitcherState(String elementName, boolean expected) {
        if (isChecked(elementInputInCheckboxPage("example", elementName)) != expected) {
            click(elementLabelInCheckboxPage("example", elementName));
        }
        return this;
    }

    public CheckboxPage verifySwitcherState(String elementName, boolean expected) {
        boolean actual = isChecked(elementInputInCheckboxPage("example", elementName));
        assertEquals(actual, expected);
        return this;
    }

}
