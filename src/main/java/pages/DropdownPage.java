package pages;

import core.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DropdownPage  extends BasePage {

    public DropdownPage(WebDriver driver) {
        super(driver);
    }

    public DropdownPage step (String step) {
        stepAction(step);
        return this;
    }

    private By dropdownIconInFirstDropDownInSelectionSection () {
        return By.xpath("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//*[@class='dropdown icon']");
    }

    private By dropdownItemInFirstDropDownInSelectionSection (String gender) {
        return By.xpath(String.format("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//*[@class='item' and normalize-space(.)='%s']", gender));
    }

    private By selectedDropdownItemInFirstDropDownInSelectionSection (String gender) {
        return By.xpath(String.format("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//div[@class='text' and normalize-space(.)='%s']", gender));
    }

    private By dropdownIconInSecondDropDownInSelectionSection () {
        return By.xpath("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]/following-sibling::*[1]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//*[@class='dropdown icon']");
    }

    private By dropdownItemInSecondDropDownInSelectionSection (String gender) {
        return By.xpath(String.format("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]/following-sibling::*[1]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//*[@class='item' and normalize-space(.)='%s']", gender));
    }

    private By selectedDropdownItemInSecondDropDownInSelectionSection (String gender) {
        return By.xpath(String.format("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]/following-sibling::*[1]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//div[@class='text' and normalize-space(.)='%s']", gender));
    }

    private By dropdownIconInSelectFriendDropDownInSelectionSection () {
        return By.xpath("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]/following-sibling::*[2]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//*[@class='dropdown icon']");
    }

    private By dropdownItemInSelectFriendDropDownInSelectionSection (String friend) {
        return By.xpath(String.format("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]/following-sibling::*[2]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//*[@class='item' and normalize-space(.)='%s']", friend));
    }

    private By selectedDropdownItemInSelectFriendDropDownInSelectionSection (String friend) {
        return By.xpath(String.format("//*[@class = 'dropdown example' and .//*[normalize-space(.)='Selection']]/following-sibling::*[2]//*[contains(@class, 'dropdown') and contains(@class, 'selection')]//div[@class='text' and normalize-space(.)='%s']", friend));
    }

    public DropdownPage selectGenderInFirstDropDownInSelectionSection (String gender) {
        click(dropdownIconInFirstDropDownInSelectionSection());
        click(dropdownItemInFirstDropDownInSelectionSection(gender));
        return this;
    }

    public DropdownPage selectGenderInSecondDropDownInSelectionSection (String gender) {
        click(dropdownIconInSecondDropDownInSelectionSection());
        click(dropdownItemInSecondDropDownInSelectionSection(gender));
        return this;
    }

    public DropdownPage selectFriendItemDropDownInSelectionSection (String friend) {
        click(dropdownIconInSelectFriendDropDownInSelectionSection());
        click(dropdownItemInSelectFriendDropDownInSelectionSection(friend));
        return this;
    }

    public DropdownPage verifyGenderInFirstDropDownInSelectionSectionIsSelected (String gender) {
        assertTrue(isDisplayed(selectedDropdownItemInFirstDropDownInSelectionSection(gender)));
        return this;
    }

    public DropdownPage verifyGenderInSecondDropDownInSelectionSectionIsSelected (String gender) {
        assertTrue(isDisplayed(selectedDropdownItemInSecondDropDownInSelectionSection(gender)));
        return this;
    }

    public DropdownPage verifyFriendInInSelectFriendDropDownInSelectionSectionIsSelected (String friend) {
        assertTrue(isDisplayed(selectedDropdownItemInSelectFriendDropDownInSelectionSection(friend)));
        return this;
    }


}
