package pages;

import core.BasePage;
import core.RightMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TablePage extends BasePage {

    public TablePage(WebDriver driver) {
        super(driver);
    }

    private By statusInTable(String tableName, String name) {
        return By.xpath(String.format("//*[@class = 'example' and .//*[normalize-space(.)='%s']]//td[normalize-space(.)='%s']//following::td[1]", tableName, name));
    }

    private By notesInTable(String tableName, String name) {
        return By.xpath(String.format("//*[@class = 'example' and .//*[normalize-space(.)='%s']]//td[normalize-space(.)='%s']//following::td[2]", tableName, name));
    }

    private By warningIconInStatusFieldInTable(String tableName, String name) {
        return By.xpath(String.format("//*[@class = 'example' and .//*[normalize-space(.)='%s']]//td[normalize-space(.)='%s']//following::td[1]//i[contains(@class,'attention icon')]", tableName, name));
    }

    private By warningIconInNotesFieldInTable(String tableName, String name) {
        return By.xpath(String.format("//*[@class = 'example' and .//*[normalize-space(.)='%s']]//td[normalize-space(.)='%s']//following::td[2]//i[contains(@class,'attention icon')]", tableName, name));
    }

    public TablePage step (String step) {
        stepAction(step);
        return this;
    }

    public TablePage verifyStatusInTable(String tableName, String name, String expectedStatus) {
        String actualStatus = getText(statusInTable(tableName, name));
        assertEquals(actualStatus, expectedStatus);
        return this;
    }

    public TablePage verifyNotesInTable(String tableName, String notes, String expectedNotes) {
        String actualNotes = getText(notesInTable(tableName, notes));
        assertEquals(actualNotes, expectedNotes);
        return this;
    }

    public TablePage verifyWarningIconExistInStatusField (String tableName, String name) {
        assertTrue(isDisplayed(warningIconInStatusFieldInTable(tableName, name)));
        return this;
    }

    public TablePage verifyWarningIconExistInNotesField (String tableName, String name) {
        assertTrue(isDisplayed(warningIconInNotesFieldInTable(tableName, name)));
        return this;
    }

    public TablePage clickDropDownIconTitleCategory(String categoryTitleName) {
        click(
                RightMenu.categoryTitle(categoryTitleName)
        );
        return this;
    }

    public TablePage selectCategory(String categoryName) {
        click(
                RightMenu.category(categoryName)
        );
        return this;
    }

}