package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    private By menuIcon() {
        return By.xpath("//a[@class='view-ui item']");
    }

    private By menuItem (String menuItem) {
        return By.xpath(String.format("//div[contains(@class,'sidebar menu')]//a[@class='item' and normalize-space(.)='%s']", menuItem));
    }

    public HomePage step (String step) {
        stepAction(step);
        return this;
    }

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openPage (String menuItem) {
        click(menuIcon());
        sleep(1000L);
        click(menuItem(menuItem));
    }

    public TablePage openPageTable () {
        openPage("Table");
        return new TablePage(driver);
    }

    public DropdownPage openPageDropdown () {
        openPage("Dropdown");
        return new DropdownPage(driver);
    }

    public CheckboxPage openPageCheckbox () {
        openPage("Checkbox");
        return new CheckboxPage(driver);
    }

}
