package tests;

import core.BaseTest;
import org.junit.Test;

public class TestThird extends BaseTest {
    @Test
    public void testThird () {
        String makeMyProfileVisible = "Make my profile visible";
        String onceAWeek = "Once a week";
        String twoThreeTimesAWeek = "2-3 times a week";
        String onceADay = "Once a day";
        String twiceADay = "Twice a day";
        String acceptTermsAndConditions = "Accept terms and conditions";
        String twentyMbpsMax = "20 mbps max";
        String tenMbpsMax = "10mbps max";
        String fiveMbpsMax = "5mbps max";
        String unmetered = "Unmetered";
        String subscribeToWeeklyNewsletter = "Subscribe to weekly newsletter";

        homePage
            .step("1")
                .openPageCheckbox()
            .step("2")
                .setCheckboxState(makeMyProfileVisible, true)
                .verifyCheckboxState(makeMyProfileVisible, true)
                .setCheckboxState(makeMyProfileVisible, false)
                .verifyCheckboxState(makeMyProfileVisible, false)

                .selectRadioButtonInHorizontalSection(onceAWeek)
                .verifyRadioButtonInHorizontalSectionSelected(onceAWeek, true)
                .verifyRadioButtonInHorizontalSectionSelected(twoThreeTimesAWeek, false)
                .verifyRadioButtonInHorizontalSectionSelected(onceADay, false)
                .verifyRadioButtonInHorizontalSectionSelected(twiceADay, false)

                .selectRadioButtonInHorizontalSection(twoThreeTimesAWeek)
                .verifyRadioButtonInHorizontalSectionSelected(onceAWeek, false)
                .verifyRadioButtonInHorizontalSectionSelected(twoThreeTimesAWeek, true)
                .verifyRadioButtonInHorizontalSectionSelected(onceADay, false)
                .verifyRadioButtonInHorizontalSectionSelected(twiceADay, false)

                .selectRadioButtonInHorizontalSection(onceADay)
                .verifyRadioButtonInHorizontalSectionSelected(onceAWeek, false)
                .verifyRadioButtonInHorizontalSectionSelected(twoThreeTimesAWeek, false)
                .verifyRadioButtonInHorizontalSectionSelected(onceADay, true)
                .verifyRadioButtonInHorizontalSectionSelected(twiceADay, false)

                .selectRadioButtonInHorizontalSection(twiceADay)
                .verifyRadioButtonInHorizontalSectionSelected(onceAWeek, false)
                .verifyRadioButtonInHorizontalSectionSelected(twoThreeTimesAWeek, false)
                .verifyRadioButtonInHorizontalSectionSelected(onceADay, false)
                .verifyRadioButtonInHorizontalSectionSelected(twiceADay, true)

                .selectRadioButtonInVerticalSection(onceAWeek)
                .verifyRadioButtonInVerticalSectionSelected(onceAWeek, true)
                .verifyRadioButtonInVerticalSectionSelected(twoThreeTimesAWeek, false)
                .verifyRadioButtonInVerticalSectionSelected(onceADay, false)
                .verifyRadioButtonInVerticalSectionSelected(twiceADay, false)

                .selectRadioButtonInVerticalSection(twoThreeTimesAWeek)
                .verifyRadioButtonInVerticalSectionSelected(onceAWeek, false)
                .verifyRadioButtonInVerticalSectionSelected(twoThreeTimesAWeek, true)
                .verifyRadioButtonInVerticalSectionSelected(onceADay, false)
                .verifyRadioButtonInVerticalSectionSelected(twiceADay, false)

                .selectRadioButtonInVerticalSection(onceADay)
                .verifyRadioButtonInVerticalSectionSelected(onceAWeek, false)
                .verifyRadioButtonInVerticalSectionSelected(twoThreeTimesAWeek, false)
                .verifyRadioButtonInVerticalSectionSelected(onceADay, true)
                .verifyRadioButtonInVerticalSectionSelected(twiceADay, false)

                .selectRadioButtonInVerticalSection(twiceADay)
                .verifyRadioButtonInVerticalSectionSelected(onceAWeek, false)
                .verifyRadioButtonInVerticalSectionSelected(twoThreeTimesAWeek, false)
                .verifyRadioButtonInVerticalSectionSelected(onceADay, false)
                .verifyRadioButtonInVerticalSectionSelected(twiceADay, true)

                .setSwitcherState(acceptTermsAndConditions, true)
                .verifySwitcherState(acceptTermsAndConditions, true)
                .setSwitcherState(acceptTermsAndConditions, false)
                .verifySwitcherState(acceptTermsAndConditions, false)

                .selectRadioButtonInVerticalSection(twentyMbpsMax)
                .verifyRadioButtonInVerticalSectionSelected(twentyMbpsMax, true)
                .verifyRadioButtonInVerticalSectionSelected(tenMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(fiveMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(unmetered, false)

                .selectRadioButtonInVerticalSection(tenMbpsMax)
                .verifyRadioButtonInVerticalSectionSelected(twentyMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(tenMbpsMax, true)
                .verifyRadioButtonInVerticalSectionSelected(fiveMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(unmetered, false)

                .selectRadioButtonInVerticalSection(fiveMbpsMax)
                .verifyRadioButtonInVerticalSectionSelected(twentyMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(tenMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(fiveMbpsMax, true)
                .verifyRadioButtonInVerticalSectionSelected(unmetered, false)

                .selectRadioButtonInVerticalSection(unmetered)
                .verifyRadioButtonInVerticalSectionSelected(twentyMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(tenMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(fiveMbpsMax, false)
                .verifyRadioButtonInVerticalSectionSelected(unmetered, true)

                .setSwitcherState(subscribeToWeeklyNewsletter, true)
                .verifySwitcherState(subscribeToWeeklyNewsletter, true)
                .setSwitcherState(subscribeToWeeklyNewsletter, false)
                .verifySwitcherState(subscribeToWeeklyNewsletter, false)
        ;

    }

}
