package tests;

import core.BaseTest;
import org.junit.Test;

public class TestSecond extends BaseTest {

    @Test
    public void testSecond () {
        String male = "Male";
        String female = "Female";
        String friend = "Christian";
        homePage
            .step("1")
                .openPageDropdown()
            .step("2")
                .selectGenderInFirstDropDownInSelectionSection(female)
                .selectGenderInSecondDropDownInSelectionSection(male)
                .selectFriendItemDropDownInSelectionSection(friend)
                .verifyGenderInFirstDropDownInSelectionSectionIsSelected(female)
                .verifyGenderInSecondDropDownInSelectionSectionIsSelected(male)
                .verifyFriendInInSelectFriendDropDownInSelectionSectionIsSelected(friend);
    }
}
