package tests;

import core.BaseTest;
import org.junit.Test;

public class TestFirst extends BaseTest {

    @Test
    public void testFirst() {
        homePage
            .step("1")
                .openPageTable()
            .step("2")
                .verifyStatusInTable("Positive / Negative", "Jimmy", "Cannot pull data")
            .step("3")
                .verifyStatusInTable("Positive / Negative", "No Name Specified", "Approved")
                .verifyNotesInTable("Positive / Negative", "No Name Specified", "None")
                .verifyStatusInTable("Positive / Negative", "Jill", "Approved")
                .verifyNotesInTable("Positive / Negative", "Jill", "None")
            .step("4")
                .verifyWarningIconExistInStatusField("Warning", "Jimmy")
                .verifyWarningIconExistInNotesField("Warning", "Jamie");
    }
}
