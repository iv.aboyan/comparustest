package core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExternalResource;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.WebDriver;
import pages.HomePage;


public class BaseTest {

    protected WebDriver driver;
    protected HomePage homePage;
    static Logger logger = Logger.getLogger(BaseTest.class);
    static String testCaseName;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    private ExternalResource driverRule = new WebDriverRule(this);

    public TestWatcher conditionRule = new ConditionRule();

    private TestWatcher screenshotRule = new ScreenshotRule(this);

    private RepeatTestRule.Repeat repeatRestRule = new RepeatTestRule.Repeat(3);


    @Rule
    public TestRule chain = RuleChain
            .outerRule(repeatRestRule)
            .around(conditionRule)
            .around(driverRule)
            .around(screenshotRule);

}