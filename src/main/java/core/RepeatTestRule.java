package core;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class RepeatTestRule {
    public static class Repeat implements TestRule {
        private int repeatCount;

        public Repeat(int repeatCount) {
            this.repeatCount = repeatCount;
        }

        public Statement apply(Statement base, Description description) {
            return statement(base, description);
        }

        private Statement statement(final Statement base, final Description description) {
            return new Statement() {
                @Override
                public void evaluate() throws Throwable {
                    Throwable caughtThrowable = null;
                    BaseTest.testCaseName = description.getMethodName();
                    int i;
                    for (i = 1; i <= repeatCount; i++) {
                        try {
                            base.evaluate();
                            BaseTest.logger.info(BaseTest.testCaseName + " repetition " + i + " of " + repeatCount + " completed successfully" + "\n");
                            return;
                        } catch (Throwable t) {
                            caughtThrowable = t;
                            BaseTest.logger.info(BaseTest.testCaseName + " repetition " + i + " of " + repeatCount + " completed unsuccessfully" + "\n");
                        }
                    }

                    BaseTest.testCaseName = null;

                    throw caughtThrowable;
                }
            };
        }
    }
}
