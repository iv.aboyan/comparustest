package core;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void sleep (Long time) {
        try {
            Thread.sleep(time);
            BaseTest.logger.info(BaseTest.testCaseName + " Thread sleep " + time + " milliseconds");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebElement findElement(By locator) {
        try {
            WebElement webElement = driver.findElement(locator);
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " was found");
            return webElement;
        } catch (Exception E) {
            BaseTest.logger.error(BaseTest.testCaseName + " WebElement " + locator + " was not found");
            Assert.fail();
        }
        return null;
    }

    public void click(By locator) {
        WebElement webElement = findElement(locator);
        try {
            webElement.click();
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " was clicked");
        } catch (Exception E) {
            BaseTest.logger.error(BaseTest.testCaseName + " WebElement " + locator + " was not clicked");
            Assert.fail();
        }
    }

    protected void clear(By locator) {
        WebElement webElement = findElement(locator);
        try {
            webElement.clear();
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " was cleared");
        } catch (Exception E) {
            BaseTest.logger.error(BaseTest.testCaseName + " WebElement " + locator + " was not cleared");
            Assert.fail();
        }
    }

    protected boolean isSelected(By locator) {
        WebElement webElement = findElement(locator);
        if (webElement.isSelected()) {
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " is selected");
        } else {
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " is not selected");
        }
        return findElement(locator).isSelected();
    }

    protected boolean isDisplayed(By locator) {
        WebElement webElement = findElement(locator);
        if (webElement.isDisplayed()) {
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " is displayed");
        } else {
            BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " is not displayed");
        }
        return findElement(locator).isDisplayed();
    }

    protected void sendKeys(By locator, String value) {
        WebElement webElement = findElement(locator);
        webElement.sendKeys(value);
        BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " was filled with value: " + value);
    }

    protected String getText(By locator) {
        WebElement webElement = findElement(locator);
        String text = webElement.getText();
        BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " returned text: " + text);
        return text;
    }

    protected boolean isChecked(By locator) {
        WebElement webElement = findElement(locator);
        boolean actual = webElement.isSelected();
        BaseTest.logger.info(BaseTest.testCaseName + " WebElement " + locator + " is checked: " + actual);
        return actual;
    }

    public void stepAction(String step) {
        BaseTest.logger.info(BaseTest.testCaseName + " Step: " + step);
    }

    public void assertTrue(boolean expected) {
        Assert.assertTrue(expected);
    }

    public void assertEquals(Object actual, Object expected) {
        Assert.assertEquals(actual, expected);
    }

}