package core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RightMenu extends BasePage {

    public RightMenu(WebDriver driver) {
        super(driver);
    }

    public static By category(String categoryName) {
        return By.xpath(String.format("//div[contains(@class, 'content menu')]//*[normalize-space(.)='%s']", categoryName));
    }

    public static By categoryTitle(String categoryTitleName) {
        return By.xpath(String.format("//*[contains(@class, 'title') and .//*[normalize-space(.)='%s']]//*[@class='dropdown icon']", categoryTitleName));
    }
}
