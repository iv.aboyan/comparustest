package core;

import org.junit.rules.ExternalResource;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

class WebDriverRule extends ExternalResource {
    private final BaseTest baseTest;
    String homePage = "https://semantic-ui.com/";

    public WebDriverRule(BaseTest baseTest) {
        this.baseTest = baseTest;
    }

    @Override
    public void before() {
        baseTest.driver = new ChromeDriver();
        BaseTest.logger.info(BaseTest.testCaseName + " ChromeDriver was initialized");
        baseTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        baseTest.driver.get(homePage);
        baseTest.homePage = new HomePage(baseTest.driver);
    }

    @Override
    public void after() {
        if (baseTest.driver != null) {
            baseTest.driver.quit();
        }
        BaseTest.logger.info(BaseTest.testCaseName + " ChromeDriver was closed");
    }
}
