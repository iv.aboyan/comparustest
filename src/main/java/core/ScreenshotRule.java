package core;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

class ScreenshotRule extends TestWatcher {
    private BaseTest baseTest;

    public ScreenshotRule(BaseTest baseTest) {
        this.baseTest = baseTest;
    }

    @Override
    protected void succeeded(Description description) {
        super.starting(description);
        BaseTest.logger.info(BaseTest.testCaseName + " Test was passed");
    }

    @Override
    protected void failed(Throwable e, Description description) {
        TakesScreenshot scrShot = ((TakesScreenshot) baseTest.driver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        String pattern = "yyyyMMddhhmmssS";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        String filePath = "Screenshots" + "\\" + date + ".jpeg";
        BaseTest.logger.error(BaseTest.testCaseName + " Test was failed");
        BaseTest.logger.error(BaseTest.testCaseName + " Screenshot was created: " + filePath);
        File DestFile = new File(filePath);
        try {
            FileUtils.copyFile(SrcFile, DestFile);
        } catch (Exception e1) {
            BaseTest.logger.error(BaseTest.testCaseName + "Screenshot was not created");
        }
    }
}
