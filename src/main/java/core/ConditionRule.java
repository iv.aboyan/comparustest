package core;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

class ConditionRule extends TestWatcher {

    @Override
    protected void starting(Description description) {
        BaseTest.testCaseName = description.getMethodName();
        BaseTest.logger.info(BaseTest.testCaseName + " Test was started");
    }

    @Override
    protected void finished(Description description) {
        BaseTest.logger.info(BaseTest.testCaseName + " Test was finished");
    }
}
