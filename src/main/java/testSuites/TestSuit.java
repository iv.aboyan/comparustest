package testSuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestFirst.class,
        TestSecond.class,
        TestThird.class,}
        )

public class TestSuit {

}