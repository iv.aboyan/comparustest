import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import testSuites.TestSuit;

public class Main {
    public static void main(String[] args) {
        JUnitCore junit = new JUnitCore();
        junit.addListener(new TextListener(System.out));
        junit.run(TestSuit.class);
    }
}